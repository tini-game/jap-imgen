const $ = id => document.getElementById(id);

const input = $("upload");
const canvas = $("canvas");
const ctx = canvas.getContext("2d");
input.addEventListener("change", () => {
	const file = input.files[0];

	const fr = new FileReader();
	fr.addEventListener("load", e => {
		const img = new Image();
		img.addEventListener("load", () => {
			ctx.canvas.width = img.width;
			ctx.canvas.height = img.height;
			ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
			ctx.drawImage(img, 0, 0);
		});
		img.src = e.target.result;
	});
	fr.readAsDataURL(file);
});

function convertToLevel(lvl) {
	return JSON.stringify([
		lvl,
		[-5, 5, 325, 1, 600, false, false, false, false],
		$("name").value,
	]);
}

function calculateAlpha([r1, g1, b1], a) {
	const [r2, g2, b2] = [255, 255, 255];
	a = 1 - a;

	return [r1 + a * (r2 - r1), g1 + a * (g2 - g1), b1 + a * (b2 - b1)];
}

function getBlock(data, blockId, fallback, x, y) {
	let r = data.data[x * data.width * 4 + y * 4],
		g = data.data[x * data.width * 4 + y * 4 + 1],
		b = data.data[x * data.width * 4 + y * 4 + 2],
		a = data.data[x * data.width * 4 + y * 4 + 3] / 255;
	const calc = calculateAlpha([r, g, b], a);
	r = calc[0];
	g = calc[1];
	b = calc[2];
	if (r > 250 && g > 250 && b > 250 && fallback) return 0;
	if (r < 6 && g < 6 && b < 6 && fallback) return 1;
	return [blockId, r, g, b];
}

// [51, r, g, b]
function getLevel() {
	const data = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);

	const blockId = $("bg").checked ? 74 : 51;
	const fallback = $("optimize").checked;

	if ($("mini").checked) {
		// Use mini blocks
		let level = new Array(Math.ceil(data.width / 2))
			.fill(0)
			.map(_ => new Array(Math.ceil(data.height / 2)).fill(0));

		level = level.map((col, idx2) =>
			col.map((_, idx1) => [
				73,
				getBlock(data, blockId, fallback, idx1 * 2, idx2 * 2),
				getBlock(data, blockId, fallback, idx1 * 2 + 1, idx2 * 2),
				getBlock(data, blockId, fallback, idx1 * 2, idx2 * 2 + 1),
				getBlock(data, blockId, fallback, idx1 * 2 + 1, idx2 * 2 + 1),
			])
		);

		copyToClipboard(convertToLevel(level));
	} else {
		// Default processing

		let level = new Array(data.width)
			.fill(0)
			.map(_ => new Array(data.height).fill(0));

		level = level.map((col, idx2) =>
			col.map((_, idx1) => getBlock(data, blockId, fallback, idx1, idx2))
		);

		copyToClipboard(convertToLevel(level));
	}
}

function copyToClipboard(text) {
	if (window.clipboardData && window.clipboardData.setData) {
		return clipboardData.setData("Text", text);
	} else if (
		document.queryCommandSupported &&
		document.queryCommandSupported("copy")
	) {
		var textarea = document.createElement("textarea");
		textarea.textContent = text;
		textarea.style.position = "fixed";
		document.body.appendChild(textarea);
		textarea.select();
		try {
			return document.execCommand("copy");
		} catch (ex) {
			console.warn("Copy to clipboard failed.", ex);
			return false;
		} finally {
			document.body.removeChild(textarea);
		}
	}
}
